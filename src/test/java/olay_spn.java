import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;

public class olay_spn extends olay_en{
private String uname,password;



    //step1 . Click on register
    @Test(priority=1)
    public void tc1_spn() throws InterruptedException
    {
        driver.findElement(By.xpath("//a[@href='/es-es/createprofilepage']")).click();
        String s = driver.getCurrentUrl();
        Assert.assertEquals("https://www.olay.es/es-es/createprofilepage",s);
        System.out.println("Homepage opened");

    }

    //step 3. Enter the user data and create the accounrt
    @Test(priority=8)
    public void tc2_spn() throws InterruptedException, IOException
    {
        File file =    new File("D:\\Sample1.xlsx");
        FileInputStream fs= new FileInputStream(file);
        XSSFWorkbook wb = null;
        wb = new XSSFWorkbook(fs);
        XSSFSheet s = wb.getSheet("Sheet1");
        XSSFRow row = s.getRow(1);
        String gender = row.getCell(0).getStringCellValue();
        if(gender.equals("Male"))
            driver.findElement(By.id("phdesktopbody_0_anchoridM")).click();
        if(gender.equals("Female"))
            driver.findElement(By.id("phdesktopbody_0_anchoridF")).click();
        driver.findElement(By.id("phdesktopbody_0_grs_consumer[firstname]")).sendKeys(row.getCell(1).getStringCellValue());
        driver.findElement(By.id("phdesktopbody_0_grs_consumer[lastname]")).sendKeys(row.getCell(2).getStringCellValue());
        uname = row.getCell(3).getStringCellValue();
        driver.findElement(By.id("phdesktopbody_0_grs_account[emails][0][address]")).sendKeys(uname);
        password = row.getCell(4).getStringCellValue();
        driver.findElement(By.id("phdesktopbody_0_grs_account[password][password]")).sendKeys(password);
        driver.findElement(By.id("phdesktopbody_0_grs_account[password][confirm]")).sendKeys(row.getCell(4).getStringCellValue());
        driver.findElement(By.id("phdesktopbody_0_grs_account[phones][0][fulltelephonenumber]")).sendKeys(row.getCell(8).getStringCellValue());
        Select v = new Select(driver.findElement(By.id("phdesktopbody_0_grs_consumer[birthdate][day]")));
        double birthday = row.getCell(5).getNumericCellValue();
        long bday = (long)birthday;
        String bd = Long.toString(bday);
        v.selectByVisibleText(bd);
        Select t = new Select(driver.findElement(By.id("phdesktopbody_0_grs_consumer[birthdate][month]")));
        double birthmonth = row.getCell(6).getNumericCellValue();
        long bmon = (long)birthmonth;
        String bm = Long.toString(bmon);
        t.selectByVisibleText(bm);
        Select u = new Select(driver.findElement(By.id("phdesktopbody_0_grs_consumer[birthdate][year]")));
        double birthyear = row.getCell(7).getNumericCellValue();
        long byear = (long)birthyear;
        String by = Long.toString(byear);
        u.selectByVisibleText(by);
        wb.close();
        driver.findElement(By.name("phdesktopbody_0$phdesktopbody_0_submit")).click();
        Assert.assertEquals("https://www.olay.es/es-es/loginpage/create-profile-thank-you-page",driver.getCurrentUrl());
        System.out.println("Register page validated and signin page opened.");

    }


    //step4.  Sign in with correct email and password and sign out
    @Test(priority=9)
    public void tc3_spn()
    {
        driver.findElement(By.id("phdesktopbody_0_username")).sendKeys(uname);
        driver.findElement(By.id("phdesktopbody_0_password")).sendKeys(password);
        driver.findElement(By.name("phdesktopbody_0$INICIAR SESI�N")).click();
        Assert.assertEquals("https://www.olay.es/es-es/viewprofilepage",driver.getCurrentUrl());
        System.out.println("Sign in validated with correct data");
        driver.findElement(By.id("phdesktopheader_0_phdesktopheadertop_2_LogOffLink")).click();
        driver.findElement(By.id("phdesktopheader_0_phdesktopheadertop_2_anchrContinue")).click();
        Assert.assertEquals("https://www.olay.es/es-es/loginpage",driver.getCurrentUrl());
        System.out.println("Sign-out Validated");
    }

    //step5.  incorrect password validation
    @Test(priority=10)

    public void tc4_spn()
    {
        driver.findElement(By.id("phdesktopbody_0_username")).sendKeys(uname);
        driver.findElement(By.id("phdesktopbody_0_password")).sendKeys("Incorrect Password");
        driver.findElement(By.name("phdesktopbody_0$INICIAR SESI�N")).click();
        if(driver.findElement(By.id("phdesktopbody_0_Message")).isDisplayed())
            System.out.println("Incorrect password validation Successfull");
        else
            System.out.println("Incorrect password validation Failed");
    }

    //step6. validate the forget-password page
    @Test(priority=11)
    public void tc5_spn()
    {
        driver.findElement(By.id("phdesktopbody_0_forgotpassword")).click();
        Assert.assertEquals("https://www.olay.es/es-es/forgot-password",driver.getCurrentUrl());
        System.out.println("Forgot Password Page Opened");
        driver.findElement(By.name("phdesktopbody_0$phdesktopbody_0_username")).sendKeys(uname);
        driver.findElement(By.name("phdesktopbody_1$phdesktopbody_0_username")).sendKeys(uname);
        driver.findElement(By.name("phdesktopbody_0$SIGUIENTE")).click();
        if(driver.findElement(By.id("phdesktopbody_0_afterSubmit")).isDisplayed())
            System.out.println("Forgot Password validation successfull");
        else
            System.out.println("Forgot password validation failed");
    }

    //step7. change the language to germany
    @Test(priority=12)
    public void tc6_spn() throws InterruptedException
    {
        driver.findElement(By.xpath("//a[@href='#myModal-country']")).click();
        driver.switchTo().activeElement();
        Thread.sleep(3000);
        driver.findElement(By.xpath("//a[@title='Alemania Alem�n']")).click();
        ArrayList<String> newTab = new ArrayList<String>(driver.getWindowHandles());
        driver.switchTo().window(newTab.get(2));
        Assert.assertEquals(driver.getCurrentUrl(),"https://www.olaz.de/de-de");
        System.out.println("Olay-Germany Page Opened");
    }
}

