import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class olay_en {
     public WebDriver driver;

    private String uname;

    //step1. Start chromedriver and open webpage
    @BeforeSuite
    public void tc_en_open(){
        System.setProperty("webdriver.chrome.driver", "D:\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.get("https://www.olay.co.uk/en-gb");
        driver.manage().window().maximize();
        String s = driver.getTitle();
        Assert.assertEquals("Olay: Skin Care Products & Tips for All Skin Types", s);
        System.out.println("Website Homepage Validated");
    }

    //click on register,Enter the user data and create the account
    @Test (priority=1)
    public void tc_Reg_En() throws InterruptedException {
        //closing the pop up
        Thread.sleep(3000);
        //click on register
        driver.findElement(By.xpath("//a[@href='/en-gb/createprofilepage']")).click();
        String s = driver.getCurrentUrl();
        Assert.assertEquals("https://www.olay.co.uk/en-gb/createprofilepage", s);
        System.out.println("Register Page Validated");

        //generating random email id
        WebElement emailtxt = driver.findElement(By.xpath("//input[@id='phdesktopbody_0_grs_account[emails][0][address]']"));
        Random randomGenerator = new Random();
        int randomint = randomGenerator.nextInt(1000);
        uname = "rega" + randomint + "@gmail.com";
        emailtxt.sendKeys(uname);

        //entering other details
        driver.findElement(By.id("phdesktopbody_0_grs_account[password][password]")).sendKeys("viraj1234");
        driver.findElement(By.id("phdesktopbody_0_grs_account[password][confirm]")).sendKeys("viraj1234");
        Select bd = new Select(driver.findElement(By.id("phdesktopbody_0_grs_consumer[birthdate][day]")));
        bd.selectByVisibleText("25");
        Select bm = new Select(driver.findElement(By.id("phdesktopbody_0_grs_consumer[birthdate][month]")));
        bm.selectByVisibleText("10");
        Select by = new Select(driver.findElement(By.id("phdesktopbody_0_grs_consumer[birthdate][year]")));
        by.selectByVisibleText("1996");
        driver.findElement(By.xpath("//*[@id=\"phdesktopbody_0_submit\"]")).click();
        driver.findElement(By.xpath("//a[@id='phdesktopbody_1_SiteLink']")).click();

        // validating account creation is successful or not
        String v = driver.getTitle();
        Assert.assertEquals("View Profile", v);
        System.out.println("User Registration and sign in successful");
    }



    //Checking sign in page with correct user id and password
    @Test (priority=2)
    public void tc_Login_En() {
        driver.findElement(By.id("phdesktopheader_0_phdesktopheadertop_2_LogOffLink")).click();
        driver.findElement(By.id("phdesktopheader_0_phdesktopheadertop_2_anchrContinue")).click();
        String s = driver.getTitle();
        Assert.assertEquals("Login", s);
        System.out.println("Signout successful");
        driver.findElement(By.name("phdesktopbody_0$phdesktopbody_0_username")).sendKeys(uname);
        driver.findElement(By.name("phdesktopbody_0$phdesktopbody_0_password")).sendKeys("viraj1234");
        driver.findElement(By.name("phdesktopbody_0$SIGN IN")).click();
        String v = driver.getTitle();
        Assert.assertEquals("View Profile", v);
        System.out.println("sign in successful");

    }

    //Checking sign in page with incorrect user id and password
    @Test (priority=3)
    public void tc_Login_Neg_En() {
        driver.findElement(By.id("phdesktopheader_0_phdesktopheadertop_2_LogOffLink")).click();
        driver.findElement(By.id("phdesktopheader_0_phdesktopheadertop_2_anchrContinue")).click();
        String s = driver.getTitle();
        Assert.assertEquals("Login", s);
        System.out.println("Signout successful");
        driver.findElement(By.name("phdesktopbody_0$phdesktopbody_0_username")).sendKeys(s);
        driver.findElement(By.name("phdesktopbody_0$phdesktopbody_0_password")).sendKeys("viraj123");
        driver.findElement(By.name("phdesktopbody_0$SIGN IN")).click();
        if (driver.findElement(By.id("phdesktopbody_0_Message")).getText().equals("The email and password combination you entered is incorrect. Please try again, or click the link below to create an account."))
            System.out.println("Incorrect password validation Successful");
        else
            System.out.println("Incorrect password validation Failed");
    }

    //step6. validate the forget password
    @Test (priority=4)
    public void tc_Forget_En() {
        driver.findElement(By.id("phdesktopbody_0_forgotpassword")).click();
        driver.findElement(By.name("phdesktopbody_0$phdesktopbody_0_username")).sendKeys(uname);
        driver.findElement(By.name("phdesktopbody_0$NEXT")).click();
        String ss = driver.findElement(By.id("phdesktopbody_0_afterSubmit")).getText();
        if (ss != "") {
            System.out.println("Forgot Password validation successful");
        } else
            System.out.println("Forgot password validation failed");
    }
    @Test (priority=5)
    public void tc_Forget_Neg_En() {
        driver.findElement(By.xpath("//a[@href='/en-gb/forgot-password']")).click();
        driver.findElement(By.name("phdesktopbody_0$phdesktopbody_0_username")).sendKeys("Incorrect@gmail.com");
        driver.findElement(By.name("phdesktopbody_0$NEXT")).click();
        String ss = driver.findElement(By.id("phdesktopbody_0_ErrorMessage")).getText();
        if (ss.equals("This email you entered has not been registered.")) {
            System.out.println("Negative Forget Password validation successful");
        } else
            System.out.println("Negative Forgot password validation failed");
    }
    @Test (priority=6)
    public void tc_lang() throws InterruptedException {
        driver.findElement(By.xpath("//a[@href='#myModal-country']")).click();
        driver.switchTo().activeElement();
        Thread.sleep(3000);
        String oldTab = driver.getWindowHandle();
        driver.findElement(By.xpath("//a[@title='Spain Español']")).click();
        ArrayList<String> newTab = new ArrayList<String>(driver.getWindowHandles());
        newTab.remove(oldTab);
        driver.switchTo().window(newTab.get(0));
        Assert.assertEquals(driver.getCurrentUrl(), "https://www.olay.es/es-es");
        System.out.println("Olay-Spanish Page Opened");
    }
}





